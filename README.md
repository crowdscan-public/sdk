
# CrowdScan-public
A brief description of what this project does and who it's for


## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project
```
    
## Usage/Examples

```javascript
import Component from 'my-project'

function App() {
  return <Component />
}
```


## Deployment

To deploy this project run

```bash
  npm run deploy
```


## Demo

Insert gif or link to demo


## Authors

- [@Ben Bellekens](mailto:ben.bellekens@crowdscan.be)
- [@Kaumudi Singh](mailto:kaumudi.singh@crowdscan.be)

