from distutils.core import setup

setup(
    name='sdk',
    version='0.1',
    packages=['Alerting', 'Auth', 'Data', 'Notifications'],
    url='https://crowdscan.be',
    license='Apache License',
    author='Ben Bellekens',
    author_email='info@crowdscan.be',
    description='This sdk includes the wrapper implementation of the crowdscan stack applications.'
)
