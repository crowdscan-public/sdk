from typing import List, Tuple
import time
from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
from dataclass_type_validator import dataclass_type_validator


@dataclass_json
@dataclass
class CrowdMeasurement:
    regions: List

@dataclass_json
@dataclass
class CrowdHeader:
    time: time
    environment: str
    timedelta: int

    def __post_init__(self):
        dataclass_type_validator(self)

@dataclass_json
@dataclass
class CrowdMsg:
    header: CrowdHeader
    payload: CrowdMeasurement

    def __post_init__(self):
        dataclass_type_validator(self)
